let size = 50
let grid = []
let savedGrid = []
let selected = []
let colors = {}

function setup() {
    var canvas = createCanvas(800, 600);
    canvas.parent('sketch');
    savedGrid = localStorage.getItem("saveGame")
    savedGrid = savedGrid ? JSON.parse(savedGrid) : []
    let count = 0
    colors = {
        white: color(255, 255, 255),
        black: color(0, 0, 0),
        red: color(152, 30, 50),
        yellow: color(234, 171, 0),
        grey: color(210)
    }
    let c = Object.keys(colors)
    for (let x = 0; x < width / size; x++) {
        grid[x] = []
        for (let y = 0; y < height / size; y++) {
            grid[x].push({
                value: "",// + count,
                // color: colors[c[(x + y) % c.length]]
                bg: "grey",
                text: "black"
            })
            count++
        }
    }
}

function draw() {
    background(220);
    noStroke()
    if (mouseIsPressed) {
        let x = Math.floor(mouseX / size)
        let y = Math.floor(mouseY / size)
        if (x >= 0 && x < width / size && y >= 0 && y < height / size) {
            if (keyIsPressed) {
                if (key == "Control") {
                    selected = selected.filter(e => !eq(e, { x, y }))
                }
            }
            else {
                if (selected.filter(e => eq(e, { x, y })).length == 0)
                    selected.push({ x, y })
            }
        }
    }

    textAlign(CENTER, CENTER)
    textSize(size / 1.5)
    for (let x = 0; x < width / size; x++) {
        for (let y = 0; y < height / size; y++) {
            fill(colors[grid[x][y].bg])
            rect(x * size, y * size, size, size)
            fill(colors[grid[x][y].text])
            text(grid[x][y].value, x * size + size / 2, y * size + size / 2)
        }
    }

    for (let o of selected) {
        let inflate = 10
        fill(color(0, 75, 255))
        rect(o.x * size - inflate / 2, o.y * size - inflate / 2, size + inflate, size + inflate)
    }
    for (let o of selected) {
        fill(colors[grid[o.x][o.y].bg])
        rect(o.x * size, o.y * size, size, size)
        fill(colors[grid[o.x][o.y].text])
        text(grid[o.x][o.y].value, o.x * size + size / 2, o.y * size + size / 2)
    }

    drawGrid()

}


function drawGrid() {
    strokeWeight(2)
    stroke(0)
    for (let i = 0; i <= width / size; i++) {
        line(i * size, 0, i * size, height)
    }
    for (let i = 0; i <= width / size; i++) {
        line(0, i * size, width, i * size)
    }
    noStroke()
}

function keyPressed() {
    if (key === "q")     //clear selection
        selected = []


    if (keyCode === BACKSPACE) { //delete tile
        if(selected.length != 0 ){
            grid[selected[0].x][selected[0].y] = { value: "", bg: "grey", text: "black" }
            selected = selected.slice(1) 
        }
    }

    if (key === "r")
        rotateSelection()
    if (key === "c")
        mySaveState()
    if (key === "l")
        myLoadState()

    if (key === "w")
        move({ x: 0, y: -1 })
    if (key === "s")
        move({ x: 0, y: 1 })
    if (key === "a")
        move({ x: -1, y: 0 })
    if (key === "d")
        move({ x: 1, y: 0 })

    if (key === "e") {
        if (selected.length != 0 && grid[selected[0].x][selected[0].y].value == "") {
            let n = getNew()
            grid[selected[0].x][selected[0].y].bg = n.bg
            grid[selected[0].x][selected[0].y].text = n.text
            grid[selected[0].x][selected[0].y].value = n.value

            selected = selected.filter(e => !eq(e, selected[0]))
        }
    }
}

function move(dir) {
    let newGrid = []
    for (let a of grid)
        newGrid.push(a.slice())

    for (let loc of selected) {
        let newloc = { x: loc.x + dir.x, y: loc.y + dir.y }
        if (newloc.x < 0 || newloc.x > width ||
            newloc.y < 0 || newloc.y > height ||
            (grid[newloc.x][newloc.y].value != "" && selected.filter(e => eq(e, newloc)).length == 0))
            return
        newGrid[newloc.x][newloc.y] = { ...grid[loc.x][loc.y] }
        newGrid[loc.x][loc.y] = { value: "", bg: "grey", text: "black" }
    }
    for (let loc of selected) {
        let newloc = { x: loc.x + dir.x, y: loc.y + dir.y }
        newGrid[newloc.x][newloc.y] = { ...grid[loc.x][loc.y] }
    }
    grid = newGrid
    for (let i in selected) {
        selected[i].x += dir.x
        selected[i].y += dir.y
    }
}

function rotateSelection() {
    let newGrid = []
    for (let a of grid)
        newGrid.push(a.slice())

    let avg = { x: 0, y: 0 }
    for (let loc of selected) {
        avg.x += loc.x
        avg.y += loc.y
    }
    avg.x = Math.floor(avg.x / selected.length)
    avg.y = Math.floor(avg.y / selected.length)

    for (let loc of selected) {
        let newloc = { x: (avg.y - loc.y) + avg.x, y: (loc.x - avg.x) + avg.y }
        if (newloc.x < 0 || newloc.x > width ||
            newloc.y < 0 || newloc.y > height ||
            (grid[newloc.x][newloc.y].value != "" && selected.filter(e => eq(e, newloc)).length == 0))
            return
        newGrid[newloc.x][newloc.y] = { ...grid[loc.x][loc.y] }
        newGrid[loc.x][loc.y] = { value: "", bg: "grey", text: "black" }
    }
    for (let loc of selected) {
        let newloc = { x: (avg.y - loc.y) + avg.x, y: (loc.x - avg.x) + avg.y }
        newGrid[newloc.x][newloc.y] = { ...grid[loc.x][loc.y] }
    }
    grid = newGrid
    let newSelected = []
    for (let loc of selected) {
        let newloc = { x: (avg.y - loc.y) + avg.x, y: (loc.x - avg.x) + avg.y }
        newSelected.push(newloc)
    }
    selected = newSelected.slice()
}

function mySaveState() {
    savedGrid = []
    for (let a of grid) {
        let row = []
        for (let b of a) {
            row.push({ ...b })
        }
        savedGrid.push(row.slice())
    }

    localStorage.setItem("saveGame", JSON.stringify(savedGrid))

}
function myLoadState() {
    console.log("load", savedGrid)
    grid = []
    for (let a of savedGrid) {
        let row = []
        for (let b of a) {
            row.push({ ...b })
        }
        grid.push(row.slice())
    }
}

function eq(a, b) {
    return (a.x == b.x && a.y == b.y)
}
